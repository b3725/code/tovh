# -*- coding: utf-8 -*-
"""
Created on Wed Feb  2 14:22:04 2022

@author: tbrae
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#Using opensource code to interpolate and to solve ODEs:
from scipy import interpolate
from scipy.integrate import solve_ivp

#Define constants in cgs units
#Sun mass:
m_s=z=1.98892e33
#Gravitational constant:
G=y=6.6720e-8
#Speed of light:
c=x=2.9979e10
#Baryon mass:
m_B=1.67262e-24


#Bag constant:
B=1.62295e-4


#Defining the integrand of the enthalpy:
def integrand(p_):
    return 1./(p_+rho_(p_))

#A solver for the TOV equations in the enthalpy method which returns the mass M
# in sun masses and the Radius R in km of a star for a given EOS (p_eval,rho_eval)
def TOVh(p_eval,rho_eval,sol):
    #Building the functions p(h) and rho(h):
    p_h=interpolate.interp1d(sol.y[0],p_eval)
    rho_h=interpolate.interp1d(sol.y[0],rho_eval)
    
    #Determining the initial values:
    r_0=1.2215e-8
    m_0=4*rho_c*np.pi*(r_0**3)/3
    
    #Determining the integration domain in h:
    h_0=sol.y[0][1]
    h_end=sol.y[0][-1]
    
    #Implementing the right side of the TOV equations in the enthalpy formulation 
    def TOV(h,y):
        m,r=y
        return [(-4.*np.pi*(rho_h(h))*(r**3)*(r-2.*m))/(m+4.*np.pi*(r**3)*(p_h(h))),-r*(r-2.*m)/(m+4.*np.pi*(r**3)*(p_h(h)))]
    
    #Solving the TOV equations  
    sol2=solve_ivp(TOV, [h_end,h_0], [m_0,r_0],max_step=0.001)
    return [sol2.y[0,-1],1.4767*sol2.y[1,-1]]

#Reading the EOS and cutting out the pressure and the energy density rho
EOS_table=pd.read_csv("BHBlp.csv", delimiter=" ", header=0)
EOS_table=EOS_table.to_numpy()

p=EOS_table[:,3]*(m_s**2)*(G**3)/(c**8)

rho=EOS_table[:,1]*m_B*(m_s**2)*(G**3)*(1+EOS_table[:,2]*(m_s**2)*(G**3)/(c**6))/(c**6)

#Building the functions rho(p) and p(rho)
rho_=interpolate.interp1d(p,rho)
p_=interpolate.interp1d(rho,p)

#Building the mass-radius values, that show up in the M-R-diagram
m_0=np.zeros(15)
r_0=np.zeros(15)
for i in range (1,15):
    #Determining the inital values for pressure and density:
    p_c=p[-i]
    rho_c=rho[-i]
    #Initial value for the enthalpy:
    h_0=0.001
    #Determining the p-rho-domain for each inital value:
    p_eval=p[:-i]
    rho_eval=rho[:-i]
    #Determing the enthalpy h:
    sol=solve_ivp(integrand, [p_eval[0],p_eval[-1]], [h_0],t_eval=p_eval)
    h_end=sol.y[0][-1]
    #Determining the mass and radius for each inital value:
    M,R=TOVh(p_eval,rho_eval,sol)
    print(M)
    print(R)
    m_0[i]=np.abs(M)
    r_0[i]=np.abs(R)
    

EOS_table=pd.read_csv("BSK21.csv", delimiter=" ", header=0)
EOS_table=EOS_table.to_numpy()

p=EOS_table[:,3]*(m_s**2)*(G**3)/(c**8)

rho=EOS_table[:,1]*m_B*(m_s**2)*(G**3)*(1+EOS_table[:,2]*(m_s**2)*(G**3)/(c**6))/(c**6)

rho_=interpolate.interp1d(p,rho)
p_=interpolate.interp1d(rho,p)

m_1=np.zeros(286-50+1)
r_1=np.zeros(286-50+1)
for i in range (50,286):
    p_c=p[-i]
    rho_c=rho[-i]
    h_0=0.001
    p_eval=p[:-i]
    rho_eval=rho[:-i]
    sol=solve_ivp(integrand, [p_eval[0],p_eval[-1]], [h_0],t_eval=p_eval)
    h_end=sol.y[0][-1]
    M,R=TOVh(p_eval,rho_eval,sol)
    print(M)
    print(R)
    m_1[i-(50-1)]=np.abs(M)
    r_1[i-(50-1)]=np.abs(R)

EOS_table=pd.read_csv("DD2_beta_T001.csv", delimiter=" ", header=0)
EOS_table=EOS_table.to_numpy()

p=EOS_table[:,3]*(m_s**2)*(G**3)/(c**8)

rho=EOS_table[:,1]*m_B*(m_s**2)*(G**3)*(1+EOS_table[:,2]*(m_s**2)*(G**3)/(c**6))/(c**6)

rho_=interpolate.interp1d(p,rho)
p_=interpolate.interp1d(rho,p)

m_2=np.zeros(28)
r_2=np.zeros(28)
for i in range (1,28):
    p_c=p[-i]
    rho_c=rho[-i]
    h_0=0.001
    p_eval=p[:-i]
    rho_eval=rho[:-i]
    sol=solve_ivp(integrand, [p_eval[0],p_eval[-1]], [h_0],t_eval=p_eval)
    h_end=sol.y[0][-1]
    M,R=TOVh(p_eval,rho_eval,sol)
    print(M)
    print(R)
    m_2[i]=np.abs(M)
    r_2[i]=np.abs(R)
    
EOS_table=pd.read_csv("GNH3.csv", delimiter=" ", header=0)
EOS_table=EOS_table.to_numpy()

p=EOS_table[:,3]*(m_s**2)*(G**3)/(c**8)

rho=EOS_table[:,1]*m_B*(m_s**2)*(G**3)*(1+EOS_table[:,2]*(m_s**2)*(G**3)/(c**6))/(c**6)

rho_=interpolate.interp1d(p,rho)
p_=interpolate.interp1d(rho,p)

m_3=np.zeros(34)
r_3=np.zeros(34)
for i in range (1,34):
    p_c=p[-i]
    rho_c=rho[-i]
    h_0=0.001
    p_eval=p[:-i]
    rho_eval=rho[:-i]
    sol=solve_ivp(integrand, [p_eval[0],p_eval[-1]], [h_0],t_eval=p_eval)
    h_end=sol.y[0][-1]
    M,R=TOVh(p_eval,rho_eval,sol)
    print(M)
    print(R)
    m_3[i]=np.abs(M)
    r_3[i]=np.abs(R)
    
EOS_table=pd.read_csv("Hempel_SFHo.csv", delimiter=" ", header=0)
EOS_table=EOS_table.to_numpy()

p=EOS_table[:,3]*(m_s**2)*(G**3)/(c**8)

rho=1e39*EOS_table[:,1]*m_B*(m_s**2)*(G**3)*(1+EOS_table[:,2]*(m_s**2)*(G**3)/(c**6))/(c**6)

rho_=interpolate.interp1d(p,rho)
p_=interpolate.interp1d(rho,p)

m_4=np.zeros(210-5+1)
r_4=np.zeros(210-5+1)
for i in range (5,210):
    p_c=p[-i]
    rho_c=rho[-i]
    h_0=0.001
    p_eval=p[:-i]
    rho_eval=rho[:-i]
    sol=solve_ivp(integrand, [p_eval[0],p_eval[-1]], [h_0],t_eval=p_eval)
    h_end=sol.y[0][-1]
    M,R=TOVh(p_eval,rho_eval,sol)
    print(M)
    print(R)
    m_4[i-(5-1)]=np.abs(M)
    r_4[i-(5-1)]=np.abs(R)
    

EOS_table=pd.read_csv("SLy4.csv", delimiter=" ", header=0)
EOS_table=EOS_table.to_numpy()

p=EOS_table[:,3]*(m_s**2)*(G**3)/(c**8)

rho=EOS_table[:,1]*m_B*(m_s**2)*(G**3)*(1+EOS_table[:,2]*(m_s**2)*(G**3)/(c**6))/(c**6)

rho_=interpolate.interp1d(p,rho)
p_=interpolate.interp1d(rho,p)

m_5=np.zeros(24)
r_5=np.zeros(24)
for i in range (1,24):
    p_c=p[-i]
    rho_c=rho[-i]
    h_0=0.001
    p_eval=p[:-i]
    rho_eval=rho[:-i]
    sol=solve_ivp(integrand, [p_eval[0],p_eval[-1]], [h_0],t_eval=p_eval)
    h_end=sol.y[0][-1]
    M,R=TOVh(p_eval,rho_eval,sol)
    print(M)
    print(R)
    m_5[i]=np.abs(M)
    r_5[i]=np.abs(R)

#pwp-EOS:    
p_c=np.logspace(-6,-2,100)
#Transition pressure:
p_t=2.5*1.62295e-4/(56.25*8)
#Constant K:
K_0=K=6.59e13*(x*x/(z*y))**(3.*(4/3-1.))/(x*x*(z)**(1.-4/3))
m_9=np.zeros_like(p_c)
r_9=np.zeros_like(p_c)


for i in range(0,len(p_c)):
    p=np.logspace(-12,np.log(p_c[i])/np.log(10),100000)
    print(p)
    
    rho=np.zeros_like(p)
    
    for j in range(0,len(p)):
        if (p[j]>=p_t):
            gamma=2.905
            K=p_t*((p_t/K_0)**(3/4)+p_t*2.5)**(-2.905)
            rho[j]=(p[j]/K)**(1./gamma)+p[j]/(gamma-1.)
        else:
            gamma=4/3
            K=K_0
            rho[j]=(p[j]/K)**(1./gamma)+p[j]/(gamma-1.)
    
    rho_=interpolate.interp1d(p,rho,bounds_error=False,kind='cubic')
    p_=interpolate.interp1d(rho,p,bounds_error=False,kind='cubic')

    sol=solve_ivp(integrand, [p[1],p[-1]], [2e-10],t_eval=p[1:-1])    
        
    M,R=TOVh(p[1:-1],rho[1:-1],sol)
    print(M)
    print(R)
    m_9[i]=np.abs(M)
    r_9[i]=np.abs(R)

#Bag model
p_c=np.logspace(-6,-2,60)
m_10=np.zeros_like(p_c)
r_10=np.zeros_like(p_c)
for i in range(0,len(p_c)):
    p=np.logspace(-12,np.log(p_c[i])/np.log(10),100000)
    print(p)
    rho=4*B+3*p
    rho_=interpolate.interp1d(p,rho)
    p_=interpolate.interp1d(rho, p)
    sol=solve_ivp(integrand, [p[1],p[-1]], [2e-10],t_eval=p[1:])
    M,R=TOVh(p[1:],rho[1:],sol)
    print(M)
    print(R)
    m_10[i]=np.abs(M)
    r_10[i]=np.abs(R)
    

    
#Implementing the experimental data for comparison:
x_6=np.linspace(6,22,10000)
#J1748-2021B:
y_6=np.full(10000,2.74)
y_6_err=[0.21]
#4U1700-377:
y_7=np.full(10000,2.44)
y_7_err=[0.27]
#B1957+20:
y_8=np.full(10000,2.39)
y_8_err=[[0.29],[0.36]]
#J0740-6620:
a=[12.35]
b=[2.08]
c=[0.75]
c_1=[0.07]
#MultiMessGW170817
d=[11.0]
e=[1.4]
f=[[0.6],[0.9]]
#J1614-2230:
y_9=np.full(10000,1.97)
y_9_err=[0.04]
#Error points for the supermassive NS:
g_1=[19.3,19.0,18.7,18.4]
i_1=[2.74,2.44,2.39,1.97]

#Determining the maximum mass for each EOS:
print("BHBlp: ")
print(np.amax(m_0[1:]))
print("BSK21: ")
print(np.amax(m_1[1:]))
print("DD2: ")
print(np.amax(m_2[1:]))
print("GNH3: ")
print(np.amax(m_3[1:]))
print("Hempel: ")
print(np.amax(m_4[1:]))
print("Sly4: ")
print(np.amax(m_5[1:]))
print("Bag model: ")
print(np.amax(m_10[1:]))
print("pwp: ")
print(np.amax(m_9[1:]))

    
#Plotting routine:
plt.plot(r_0[1:],m_0[1:],'m',label='BHBlp')
plt.plot(r_1[1:],m_1[1:],'g',label='BSK21')
plt.plot(r_2[1:],m_2[1:],'b',label='DD2')
plt.plot(r_3[1:],m_3[1:],'r',label='GNH3')
plt.plot(r_4[1:],m_4[1:],'y',label='Hempel')
plt.plot(r_5[1:],m_5[1:],'c',label='SLy4')
plt.scatter(g_1,i_1,color="black",s=10)
plt.plot(x_6[500:],y_6[500:],'k',linestyle='dashed')
plt.errorbar(19.3,2.74,yerr=y_6_err,fmt='none',color='black',capsize=4)
plt.plot(x_6[20:],y_7[20:],'k',linestyle='dashed')
plt.errorbar(19.0,2.44,yerr=y_7_err,fmt='none',color='black',capsize=4)
plt.plot(x_6,y_8,'k',linestyle='dashed')
plt.errorbar(18.7,2.39,yerr=y_8_err,fmt='none',color='black',capsize=4)
plt.plot(x_6,y_9,'k',linestyle='dashed')
plt.errorbar(18.4,1.97,yerr=y_9_err,fmt='none',color='black',capsize=4)
plt.plot(r_9,m_9,color="peru",linestyle='dashed',label='pwp')
plt.plot(r_10,m_10,color='indigo',linestyle='dashdot',label='Bag model')
y_20=4*x_6/(9*1.4767)
plt.fill_between(x_6,y_20,np.max(y_20),color='black')
plt.scatter(a,b,color="black",s=15)
plt.errorbar(a,b,xerr=c,yerr=c_1,fmt='none',color="black",capsize=4)
plt.scatter(d,e,color="black",s=15)
plt.errorbar(d,e,xerr=f,fmt='none',color="black",capsize=4)
plt.xlabel("Radius $R$ [km]")
plt.ylabel("Mass $M$ $[M_\odot]$")
plt.xlim(6,22)
plt.ylim(0,3.1)
plt.text(19.8,2.44+0.08,"4U 1700-377",fontsize="7.5",color="black")
plt.text(19.8,2.74+0.08,"J1748-2021B",fontsize="7.5",color="black")
plt.text(19.8,2.39-0.1,"B1957+20",fontsize="7.5",color="black")
plt.text(14.2,2.08,"J0740-6620",fontsize="7.5",color='black')
plt.text(8.2,1.4,"GW170817",fontsize="7.5",color="black")
plt.text(19.8,1.97-0.1,"J1614-2230",fontsize="7.5",color='black')
plt.grid()
plt.legend(loc="lower right")
plt.savefig("Allezusammen.png",dpi=300)
plt.show()
