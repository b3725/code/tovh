# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 14:16:13 2021

@author: tbrae
"""

import matplotlib.pyplot as plt
import numpy as np

#Define constants in cgs units
#Sun mass:
m_s=1.98892e33
#Gravitational constant:
G=6.6720e-8
#Speed of light:
c=2.9979e10
#Baryon mass:
m_B=1.67262e-24


x=np.linspace(0,13,100000)

#The values for the number density of baryons n_B [fm^-3]
d=5.260730e+38
#the mass density rho [g/cm^3]:
a=9.861244e+14
#and the pressure in [dyne/cm^2]:
b=1.914630e+35

#Computing the energy density e_0=m_B*n_B*(1+rho) and the pressure P_0 in geom. units:
e_0=d*m_B*(m_s**2)*(G**3)*(1.+a*(m_s**2)*(G**3)/(c**6))/(c**6)
P_0=b*(m_s**2)*(G**3)/(c**8)

print(e_0)
print(P_0)


#Computing the two bounds with x in [km]
y1=(4/3)*e_0*np.pi*x*x*x/(1.4767**3)
y2=(2/9)*x*(1-6*np.pi*P_0*x*x/(1.4767**2)+np.sqrt(1+6*np.pi*P_0*x*x/(1.4767**2)))/1.4767

#Rhoades-Ruffini:
y3=np.full(100000,3.2)

#Plotting routine:
plt.figure()
plt.plot(x,y1,label='lower bound')
plt.plot(x,y2,label='upper bound')
plt.fill_between(x,y3, y1, where=(y3>y1)&(y2>y3), color='green', label='permitted values')
plt.fill_between(x,y2,y1,where=(y2>y1)&(y2<y3),color='green')
plt.plot(x,y3,linestyle='dashed',label='Rhoades-Ruffini',color='black')
plt.xlabel('Radius $r_0$ $[km]$')
plt.ylabel('Mass $m_0$ $[M_{\odot}]$')
plt.grid(True)
plt.legend()
plt.savefig("BSK21e14Neu.png",dpi=300)
plt.show()