# -*- coding: utf-8 -*-
"""
Created on Thu Feb 24 14:24:54 2022

@author: tbrae
"""
import numpy as np
import matplotlib.pyplot as plt

#Define constants in cgs units:
#Speed of light c:
x=2.9979e10
#Gravitational constant G:
y=6.672e-8
#Sun mass:
z=1.98892e33
#Bag constant:
B=1.62295e-4

#Computing K in geom. units with K=3.59e13 in cgs units:
K_0=K=3.59e13*(x*x/(z*y))**(3.*(4/3-1.))/(x*x*(z)**(1.-4/3))

x=np.logspace(-7,-3,100000)
#Degenerate electron gas:
y1=K*x**(4/3)
#Bag model:
y2=(x-4*B)/3


#Plotting routine:
plt.plot(x,y1,'b',label='Polytrope')
plt.plot(x[95309:95482],y2[95309:95482],'r',label='Bag model')
plt.xlabel('Density ρ')
plt.ylabel('Pressure P')
plt.text(0.00066,0.1e-6,"ρ=4B",fontsize="8.5",color='black')
plt.grid()
plt.legend()
plt.savefig("PolyVsBagSketchTest1.png",dpi=300)
plt.show()

